'use strict';

var Twitter = require('twitter'),
	React = require('react'),
	ReactDOMServer = require('react-dom/server'),
	Q = require('q'),
	client;

// Allows for requiring of jsx files
require('node-jsx').install({
	extension: '.jsx'
});

client = new Twitter({
	consumer_key: process.env.TWITTER_CONSUMER_KEY,
	consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
	access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
	access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET,
});

function _fetchTweets(handle) {
	return Q.promise(function(resolve, reject) {
		client.get('statuses/user_timeline', {
			count: 10,
			screen_name: handle
		}, function(error, tweets){
			error ? reject(error) : resolve(tweets);
		});
	});
}

function renderTweets(req, res) {
	var TweetList = require('../../client/scripts/components/TweetList');

	_fetchTweets(req.params.handle).then(function(tweets) {
		var props = {
			handle: req.params.handle,
			tweets: tweets
		};

		res.render('index', {
			props: JSON.stringify(props),
			reactHtml: ReactDOMServer.renderToString(React.createElement(TweetList, props))
		});
	}, function() {
		res.send('There appears to have been a problem!');
	});
}

function getTweets(req, res) {
	_fetchTweets(req.params.handle).then(function(tweets) {
		res.json(tweets);
	}, function(error) {
		res.status(500).json(error);
	});
}

module.exports = {
	renderTweets: renderTweets,
	getTweets: getTweets
};

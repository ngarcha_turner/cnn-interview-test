'use strict';

var express = require('express'),
	process = require('../process'),
	router = express.Router();

router.get('/:handle([^\-]+)-tweets', process.renderTweets);
router.get('/tweets/:handle', process.getTweets);

// Fallback to CNN Breaking News tweets
router.get('*', function(req, res) {
	res.redirect('/cnnbrk-tweets');
});

module.exports = router;

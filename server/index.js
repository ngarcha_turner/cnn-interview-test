'use strict';

var express = require('express'),
	logger = require('morgan'),
	fs = require('fs'),
	path = require('path'),
	routes = require('./routes'),
	app = express(),
	port = process.env.PORT || 3000;

// Setup logging/error file logging
app.use(logger('common', {
	stream: fs.createWriteStream(__dirname + '/error.log', {
		flags: 'a'
	}),
	skip: function(req, res) {
		return res.statusCode < 400;
	}
}));

app.set('view engine', 'ejs');
app.set('views', path.resolve(__dirname, './views'));
app.use(express.static(path.resolve(__dirname, '../public')));

// Apply route functions
app.use('/', routes);

// Start service
app.listen(port, function() {
	console.log('localhost listening on port: ' + port);
});

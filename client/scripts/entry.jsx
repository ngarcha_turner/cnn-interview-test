'use strict';

var React = require('react'),
	ReactDOM = require('react-dom'),
	TweetList = require('./components/TweetList');

require('styles/main.scss');

ReactDOM.render(
	<TweetList {...window.__INITIAL_PROPS__} />,
	document.getElementById('tweets')
);

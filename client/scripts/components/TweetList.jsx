'use strict';

var React = require('react'),
	Tweet = require('./Tweet'),
	$ = require('jquery');

module.exports = React.createClass({
	propTypes: {
		handle: React.PropTypes.string.isRequired,
		tweets: React.PropTypes.arrayOf(React.PropTypes.object).isRequired
	},

	getInitialState: function() {
		return {
			tweets: this.props.tweets,
			error: false
		};
	},

	componentDidMount: function() {
		$(this.refs.tweetsList).fadeIn();

		// Make a request for new tweets every 10 seconds
		this.pollTimer = setInterval(this.getTweets, 10000);
	},

	componentWillUnmount: function() {
		this.request.abort();
		this.pollTimer = clearInterval(this.pollTimer);
	},

	getTweets: function() {
		this.request = $.get('/tweets/' + this.props.handle)
			.success(this.handleNewTweets)
			.error(this.handleRequestError);
	},

	handleNewTweets: function(response) {
		this.setState({
			tweets: response,
			error: false
		});
	},

	handleRequestError: function() {
		this.setState({
			error: true
		});
	},

	parseEntities: function(text, entities) {
		entities.urls.forEach(function(entity) {
			text = text.substring(0, entity.indices[0]) + '<a href="' + entity.url + '" target="_blank">' + entity.url + '</a>' + text.substring(entity.indices[1]);
		});

		return text;
	},

	render: function() {
		return (
			<div ref="tweetsList" className="m-tweets__list" style={{display: 'none'}}>
				<h1>Latest tweets for @{this.props.handle}</h1>

				{!this.state.error ? this.state.tweets.map(function(tweet) {
					return (
						<Tweet
							key={tweet.id}
							text={this.parseEntities(tweet.text, tweet.entities)}
							date={new Date(tweet.created_at)}
						/>
					);
				}.bind(this)) : 'There appears to have been a problem!'}
			</div>
		);
	}
});

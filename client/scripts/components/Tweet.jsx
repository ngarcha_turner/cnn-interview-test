'use strict';

var React = require('react'),
	moment = require('moment');

module.exports = React.createClass({
	propTypes: {
		text: React.PropTypes.string.isRequired,
		date: React.PropTypes.instanceOf(Date).isRequired
	},

	render: function() {
		return (
			<div className="m-tweets__tweet">
				<p className="m-tweets__tweet-copy" dangerouslySetInnerHTML={{__html: this.props.text}}></p>
				<p className="m-tweets__tweet-time">{moment(this.props.date).fromNow()}</p>
			</div>
		);
	}
});

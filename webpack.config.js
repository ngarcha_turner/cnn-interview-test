'use strict';

var path = require('path'),
	webpack = require('webpack');

module.exports = {
	devtool: 'eval',
	entry: './client/scripts/entry.jsx',

	output: {
		path: 'public/',
		filename: 'cnn-interview-test.js'
	},

	resolve: {
		extensions: ['', '.js', '.jsx'],
		root: [
			path.join(__dirname, 'client/'),
			path.join(__dirname, 'bower_components/')
		]
	},

	module: {
		loaders: [{
			test: /\.scss$/,
			loaders: ['style', 'css', 'sass?outputStyle=expanded&' +
				'includePaths[]=' + (path.resolve(__dirname, './node_modules/compass-mixins/lib')) + '&' +
				'includePaths[]=' + (path.resolve(__dirname, './bower_components')) + '&' +
				'includePaths[]=' + (path.resolve(__dirname, './node_modules'))
			],
		}, {
			test: /\.jsx?$/,
			exclude: /(node_modules|bower_components)/,
			loader: 'babel',
			query: {
				presets: ['react']
			}
		}]
	},

	plugins: [
		new webpack.ResolverPlugin(
			new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin('bower.json', ['main', ['main', 0]])
		)
	]
};
